var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var sliderValue;
var slider = document.getElementById("myRange");



function clear() {
        ctx.fillStyle = "white";
        ctx.fillRect(0,0, canvas.width, canvas.height);
    }
function slider2pressure (h, w){
  gridSquares[h][w].pressure = slider.value*10;

}

function strength2color(strength) {
    let s = 255 - strength;
    return "rgb(" + s + ", " + s + ", " + s + ")";

}
function colorChange(pressure){
   return "rgba(0, 0, 255, "+(.5 + (pressure-500)/1000)+")"
}
function makeGrid(width, height) {
    let color = 255;
    var w;
    var h;
    let grid = []
    for (h=0; h<height; h++) {
        grid.push([]);
        for (w=0; w<width; w++) {
            color = (Math.random()*255)
            grid[h][w] = {strength: color,
                         isWater: false,
                         waterDirection: "c", //c stands for center no direction
                         pressure: 0,
                         constant: false};
    }
    }
    return grid;
}

function erosion (strength, pressure,flowDirection, blockDirection){
    if (strength<=0) {
        return 0;
    }
    let c = 1;
    if (flowDirection == blockDirection) {
        c = 10;
    }
    if ((flowDirection == "n" || "s") && (blockDirection == "e" || "w")) {
        c = .1;
    }
    if ((flowDirection == "n" && blockDirection == "s") || (flowDirection == "s" && blockDirection == "n") || (flowDirection == "e" && blockDirection == "w") || (flowDirection == "w" && blockDirection == "e")) {
        c = .001;
    }
    erosionFactor = (pressure/strength)*c;
    return strength-erosionFactor;
}

gridSquares = makeGrid(100,100);
/*
gridSquares[2][0].strength = 50;
gridSquares[2][1].strength = 50;
gridSquares[2][2].strength = 50;
gridSquares[2][3].strength = 50;
gridSquares[2][4].strength = 50;
gridSquares[2][0].isWater = true;
gridSquares[2][0].constant = true;
gridSquares[2][0].pressure = 1000;
gridSquares[2][4].constant = true;
gridSquares[2][4].pressure = 0;
*/
setInterval(function() {
    clear();
    var w;
    var h;
    for (h=0; h<gridSquares.length; h++) {
        for (w=0; w<gridSquares[0].length; w++) {
            let col = -1;
            if (gridSquares[h][w].isWater) {
                col = colorChange(gridSquares[h][w].pressure);
            } else {
                col = strength2color(gridSquares[h][w].strength)
            }
            ctx.fillStyle = col;
            ctx.fillRect((canvas.width/gridSquares[0].length)*w, (canvas.height/gridSquares.length)*h, canvas.width/gridSquares[0].length, canvas.height/gridSquares.length);

            let waterLeft = false;
            let waterRight = false;
            let waterUp = false;
            let waterDown = false;


            if (w==0) {
                waterLeft = false;
            } else {
                waterLeft = gridSquares[h][w-1].isWater;
            }
            if (h==gridSquares.length-1){
                waterDown = false;
            } else {
                waterDown = gridSquares[h+1][w].isWater;
            }
            if (h==0){
                waterUp = false;
            } else {
                waterUp = gridSquares[h-1][w].isWater;
            }
            if (w==gridSquares[0].length-1){
                waterRight = false;
            } else {
                waterRight = gridSquares[h][w+1].isWater;
            }


            if (gridSquares[h][w].strength <= 0 && (waterLeft || waterRight || waterUp || waterDown)){
                gridSquares[h][w].isWater = true;
            }

            let pressureLeft = 0;
            let pressureRight = 0;
            let pressureUp = 0;
            let pressureDown = 0;


            if (w==0) {
                pressureLeft = 0;
            } else if (gridSquares[h][w].pressure > gridSquares[h][w-1].pressure && gridSquares[h][w-1].isWater == true)
            {
                pressureLeft = flowRate(gridSquares[h][w].pressure, gridSquares[h][w-1].pressure);
                if (gridSquares[h][w-1].constant == false) {
                    gridSquares[h][w-1].pressure += pressureLeft;
                }
                if (gridSquares[h][w].constant == false) {
                    gridSquares[h][w].pressure -= pressureLeft;
                }
            }
            if (h==gridSquares.length-1){
                pressureDown = 0;
            } else if(gridSquares[h][w].pressure > gridSquares[h+1][w].pressure && gridSquares[h+1][w].isWater == true) {
                pressureDown = flowRate(gridSquares[h][w].pressure, gridSquares[h+1][w].pressure);
                if (gridSquares[h+1][w].constant == false) {
                    gridSquares[h+1][w].pressure += pressureDown;
                }
                if (gridSquares[h][w].constant == false) {
                    gridSquares[h][w].pressure -= pressureDown;
                }
            }
            if (h==0){
                pressureUp = 0;
            } else if(gridSquares[h][w].pressure > gridSquares[h-1][w].pressure && gridSquares[h-1][w].isWater == true) {
                pressureUp = flowRate(gridSquares[h][w].pressure, gridSquares[h-1][w].pressure);
                if (gridSquares[h-1][w].constant == false) {
                    gridSquares[h-1][w].pressure += pressureUp;
                }
                if (gridSquares[h][w].constant == false) {
                    gridSquares[h][w].pressure -= pressureUp;
                }
            }
            if (w==gridSquares[0].length-1){
                pressureRight = 0;
            } else if(gridSquares[h][w].pressure > gridSquares[h][w+1].pressure && gridSquares[h][w+1].isWater == true) {
                pressureRight = flowRate(gridSquares[h][w].pressure, gridSquares[h][w+1].pressure);
                if (gridSquares[h][w+1].constant == false) {
                    gridSquares[h][w+1].pressure  += pressureRight;
                }
                if (gridSquares[h][w].constant == false) {
                    gridSquares[h][w].pressure -= pressureRight;
                }
            }

            if (w!=0 && gridSquares[h][w].isWater == true && gridSquares[h][w-1].strength > 0) {
                gridSquares[h][w-1].strength = erosion(gridSquares[h][w-1].strength, gridSquares[h][w].pressure, gridSquares[h][w].waterDirection, "w");
            }
            if (h!=gridSquares.length-1 && gridSquares[h][w].isWater == true && gridSquares[h+1][w].strength > 0){
                gridSquares[h+1][w].strength = erosion(gridSquares[h+1][w].strength, gridSquares[h][w].pressure, gridSquares[h][w].pressure, gridSquares[h][w].waterDirection, "s");
            }
            if (h!=0 && gridSquares[h][w].isWater == true && gridSquares[h-1][w].strength > 0){
                gridSquares[h-1][w].strength = erosion(gridSquares[h-1][w].strength, gridSquares[h][w].pressure, gridSquares[h][w].pressure, gridSquares[h][w].waterDirection, "n");
            }
            if (w!=gridSquares[0].length-1 && gridSquares[h][w].isWater == true && gridSquares[h][w+1].strength > 0){
                gridSquares[h][w+1].strength = erosion(gridSquares[h][w+1].strength, gridSquares[h][w].pressure, gridSquares[h][w].pressure, gridSquares[h][w].waterDirection, "e");
            }

            let nPressure = 1000;
            let sPressure = 1000;
            let wPressure = 1000;
            let ePressure = 1000;

            if (w!=0) {
                wPressure = gridSquares[h][w-1].pressure + gridSquares[h][w-1].strength*10;
            } else {wPressure = 100000};

            if (h!=0) {
                nPressure = gridSquares[h-1][w].pressure + gridSquares[h-1][w].strength*10;
            } else {nPressure = 100000};

            if (w!=gridSquares[0].length-1) {
                ePressure = gridSquares[h][w+1].pressure + gridSquares[h][w+1].strength*10;
            } else {ePressure = 100000};

            if (h!=gridSquares.length-1) {
                sPressure = gridSquares[h+1][w].pressure + gridSquares[h+1][w].strength*10;
            } else {sPressure = 100000};

            if (nPressure < sPressure && nPressure < ePressure && nPressure < wPressure) {
                gridSquares[h][w].waterDirection = "n";
            }
            if (sPressure < nPressure && sPressure < ePressure && sPressure < wPressure) {
                gridSquares[h][w].waterDirection = "s";
            }
            if (ePressure < nPressure && ePressure < sPressure && ePressure < wPressure) {
                gridSquares[h][w].waterDirection = "e";
            }
            if (wPressure < nPressure && wPressure < sPressure && wPressure < ePressure) {
                gridSquares[h][w].waterDirection = "w";
            }
    }
    }
}, 33);
function flowRate (p1, p2){ //p1 is pressure 1 and p2 is pressure 2
    return Math.round(1 + 0.1*(p1-p2));
}
